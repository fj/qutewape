#!/usr/bin/env python3

import base64
import os
import sys

from textwrap import dedent
from typing import Optional, TextIO


def read_css(filename: str) -> str:
    """Read a css file from the css/ directory"""

    with open(os.path.join('css/', filename)) as css_file:
        return css_file.read()


def combine_all_css() -> str:
    """Returns a combined version of all css files in the `css` directory"""

    combined = ''
    for entry in os.scandir('css/'):
        if not entry.is_file() or not entry.path.endswith('.css'):
            continue
        combined += read_css(entry.name)
    return combined


def css_injector(css: str) -> str:
    """Generate Javascript code to inject a specific CSS code """

    template = dedent("""\
        let el = document.createElement('style');
        el.innerHTML = atob('%s');
        el.id = 'qutewape';
        document.getElementsByTagName("head")[0].appendChild(el);""")
    css_b64 = base64.b64encode(css.encode()).decode()
    return template % css_b64


def css_cleaner() -> str:
    """Generate Javascript code to clean all injected CSS"""

    return dedent("""\
        let el = document.getElementById("qutewape");
        if (el) {
            el.innerHTML = '';
            el.parentNode.removeChild(el);
        }""")


def eval_js(fifo_file: TextIO, js_code: str) -> None:
    """Run Javascript code in qutebrowser using jseval"""

    js_code_b64 = base64.b64encode(js_code.encode()).decode()
    fifo_file.write(f'jseval --quiet eval(atob("{js_code_b64}"))\n')


def get_param(param_name: str) -> Optional[str]:
    """Convenient wrapper to os.getenv() for qutebrowser passed environment variables"""

    return os.getenv(f'QUTE_{param_name.upper()}')


def main(is_enabled):
    """Entry point for qutebrowser userscript"""

    with open(get_param('fifo'), 'w') as fifo_file:
        eval_js(fifo_file, css_cleaner())
        if is_enabled:
            eval_js(fifo_file, css_injector(combine_all_css()))


if __name__ == '__main__':
    if len(sys.argv) != 2 or sys.argv[1] not in ('enable', 'disable'):
        print(f'usage: {sys.argv[0]} <enable|disable>')
        sys.exit(2)
    elif not get_param('fifo'):
        print('not invoked by qutebrowser, exiting...')
        sys.exit(3)
    os.chdir(os.path.dirname(__file__))
    main(sys.argv[1] == 'enable')
