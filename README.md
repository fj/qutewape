# qutewape

WhatsApp Web privacy extension for qutebrowser. It is basically just a
userscript that injects CSS code to the WhatsApp Web page.

This project is largely inspired from
[LukasKen/Privacy-Extension-For-Whatsapp-Web](https://github.com/LukasLen/Privacy-Extension-For-WhatsApp-Web)
project. Most, if not all, of the CSS codes used in this project (included in
the `css` directory) is copied directly from the project.

## Usage

qutewape is a userscript. qutewape can be used by invoking (or spawning)
`main.py` from the WhatsApp Web page.

    :spawn path/to/main.py enable

Currently, the userscript accepts only one argument: `enable` or `disable`. It
is used respectively to inject or clean qutewape's CSS files.

Conveniently, you can also set an alias to the spawn command.

    c.aliases.update(
        qwe='spawn -u path/to/main.py enable',
        qwd='spawn -u path/to/main.py disable',
    )

Injected CSS files are located in the `css` directory. Available or not injected
CSS files are located in the `css-available` directory. You can customize which
part should be blurred (or any specific behavior) by moving/tweaking the CSS
files.

## Working Principle

1.  Read every CSS files in the `css` director and combine them into one string
    (`css-available` is a directory containing CSS files that are not injected).
2.  The string is injected to qutebrowser via `jseval` by creating a `<style>`
    element as a child of the current `<head>` element.

## Contribution

Any kind of contribution is highly appreciated ;)

## License

This project is distributed under MIT License.
